package fr.bnf.izycoopt.izycoopt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IzycooptApplication {

	public static void main(String[] args) {
		SpringApplication.run(IzycooptApplication.class, args);
		System.out.println("TEST");
	}

}
